import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrainingPage } from './training';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    TrainingPage,
  ],
  imports: [
    IonicPageModule.forChild(TrainingPage),
    ComponentsModule
  ],
})
export class TrainingPageModule {}
