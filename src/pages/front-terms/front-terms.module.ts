import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FrontTermsPage } from './front-terms';

@NgModule({
  declarations: [
    FrontTermsPage,
  ],
  imports: [
    IonicPageModule.forChild(FrontTermsPage),
  ],
})
export class FrontTermsPageModule {}
