import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NutritionRecipeDetailPage } from './nutrition-recipe-detail';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    NutritionRecipeDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(NutritionRecipeDetailPage),
    ComponentsModule,
  ],
})
export class NutritionRecipeDetailPageModule {}
