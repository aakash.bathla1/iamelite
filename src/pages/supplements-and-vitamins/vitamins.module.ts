import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VitaminsPage } from './vitamins';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    VitaminsPage,
  ],
  imports: [
    IonicPageModule.forChild(VitaminsPage),
    ComponentsModule,
  ],
})
export class VitaminsPageModule {}
