import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NutritionRecipePage } from './nutrition-recipe';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    NutritionRecipePage,
  ],
  imports: [
    IonicPageModule.forChild(NutritionRecipePage),
    ComponentsModule,
  ],
})
export class NutritionRecipePageModule {}
