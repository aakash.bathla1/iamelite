import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AccountabilityPage } from './accountability';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    AccountabilityPage,
  ],
  imports: [
    IonicPageModule.forChild(AccountabilityPage),
    ComponentsModule
  ],
})
export class AccountabilityPageModule {}
