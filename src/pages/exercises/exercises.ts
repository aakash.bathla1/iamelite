import { Component } from '@angular/core';
import { IonicPage, Loading, LoadingController, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer} from '@angular/platform-browser'

import { UserService } from './../../providers/user';
import { ContentService } from '../../providers/content';

@IonicPage()
@Component({
  selector: 'page-exercises',
  templateUrl: 'exercises.html',
})
export class ExercisesPage {
  public contents: any[];
  public loader: Loading;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public contentService: ContentService,
    public domSanitizer: DomSanitizer,
    public userService: UserService,
    public loadingCtrl: LoadingController
  ) {
    this.contents = [];
  }


  ionViewWillEnter() {
    this.contentService
      .getExerciseDemos()
      .subscribe(data => {
        this.contents = data.map(content => {
          content.videoSource = this.domSanitizer.bypassSecurityTrustResourceUrl(content.source);
          content.styleUrl = this.domSanitizer.bypassSecurityTrustStyle('url(' + content.preview + ')');
          return content;
        })
      });
  }

  ionViewDidEnter() {
    var LightTableFilter = (function(Arr) {

      var _input;

      function _onInputEvent(e) {
        _input = e.target;
        var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
        Arr.forEach.call(tables, function(table) {
          Arr.forEach.call(table.tBodies, function(tbody) {
            Arr.forEach.call(tbody.rows, _filter);
          });
        });
      }

      function _filter(row) {
        var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
        row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
      }

      return {
        init: function() {
          var inputs = document.getElementsByClassName('light-table-filter');
          Arr.forEach.call(inputs, function(input) {
            input.oninput = _onInputEvent;
          });
        }
      };
    })(Array.prototype);

    LightTableFilter.init();
  }

  gotoHomePage(): void {
    this.navCtrl.push('HomePage').then(() => this.loader.dismiss());

    this.loader = this.loadingCtrl.create();
    this.loader.present();
  }

}
