import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { UserService } from '../../providers/user';

@IonicPage()
@Component({
  selector: 'page-password-reset',
  templateUrl: 'password-reset.html',
})
export class PasswordResetPage {
  public form: FormGroup
  public loader: Loading;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public fb: FormBuilder,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private userService: UserService
  ) {
    this.form = fb.group({
      email: ['', Validators.email]
    });
  }

  gotoLoginPage(): void {
    this.navCtrl.push('LoginPage');
  }

  onSubmit() {
    if (this.form.invalid) {
      let error = 'Unable to proceed. All fields are required.';
      let info = this.toastCtrl.create({
        message: error,
        position: 'top',
        duration: 3000,
        cssClass: "dangertoaster"
      });
      info.present();

      return;
    }
    this.userService.sendPasswordResetLink(this.form.value.email)
      .subscribe(() => {
        this.loader.dismiss();
        this.navCtrl.push('LoginPage');
      }, error => {
        this.loader.dismiss();
        let info = this.toastCtrl.create({
          message: error.message,
          position: 'top',
          duration: 3000
        });
        info.present();
      });
    
    this.loader = this.loadingCtrl.create();
    this.loader.present();
  }

}
