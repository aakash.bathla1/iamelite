import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EliteDetailPage } from './elite-detail';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    EliteDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(EliteDetailPage),
    ComponentsModule,
  ],
})
export class EliteDetailPageModule {}
