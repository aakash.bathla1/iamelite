import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NutritionMealPage } from './nutrition-meal';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    NutritionMealPage,
  ],
  imports: [
    IonicPageModule.forChild(NutritionMealPage),
    ComponentsModule,
  ],
})
export class NutritionMealPageModule {}
