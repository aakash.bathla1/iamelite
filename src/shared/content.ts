export class Content {
  public id: string
  public title: string
  public content: string
  public source: string
  public sourceType: string
  public preview: string
  public styleUrl: any
  public imageSource: any
  public videoSource: any
  
  constructor(obj?: any) {
    this.id = obj && obj.id || null;
    this.title = obj && obj.title || null;
    this.content = obj && obj.content || null;
    this.source = obj && obj.source || null;
    this.sourceType = obj && obj.source_type || null;
    this.preview = obj && obj.preview || null;
  }
}