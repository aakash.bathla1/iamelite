import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer} from '@angular/platform-browser'

import { UserService } from './../../providers/user';
import { ContentService } from '../../providers/content';

@IonicPage()
@Component({
  selector: 'page-education-workout',
  templateUrl: 'education-workout.html',
})
export class EducationWorkoutPage {
  public contents: any[];
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public contentService: ContentService,
    public domSanitizer: DomSanitizer,
    public userService: UserService,
  ) {
    this.contents = [];
  }

  ionViewWillEnter() {
    this.contentService
      .getEducation('workout')
      .subscribe(data => {
        this.contents = data.map(content => {
          content.styleUrl = this.domSanitizer.bypassSecurityTrustStyle('url(' + content.preview + ')');
          return content;
        })
      })
      ;
  }

  viewPage(content): void {
    this.navCtrl.push('EducationSinglePage', {type: 'workout', content: content});
  }

  gotoHomePage(): void {
    this.userService.resetNavigationTo('home');
  }

}
