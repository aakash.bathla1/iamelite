import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer} from '@angular/platform-browser';

@IonicPage()
@Component({
  selector: 'page-elite-detail',
  templateUrl: 'elite-detail.html',
})
export class EliteDetailPage {
  public content: any;
    constructor(
      public navCtrl: NavController, 
      public navParams: NavParams,
      private domSanitizer: DomSanitizer,
    ) {
      this.content = navParams.get('content');
    }

  ionViewDidLoad() {
    this.content.videoSource = this.domSanitizer.bypassSecurityTrustResourceUrl(this.content.source);
  }

}
