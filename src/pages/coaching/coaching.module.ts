import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CoachingPage } from './coaching';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    CoachingPage,
  ],
  imports: [
    IonicPageModule.forChild(CoachingPage),
    ComponentsModule
  ],
})
export class CoachingPageModule {}
