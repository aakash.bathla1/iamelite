import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { InAppPurchase2, IAPProduct } from '@ionic-native/in-app-purchase-2';

import { Subject } from 'rxjs/Subject'
import 'rxjs/add/operator/debounceTime'

import { Config } from './../../providers/config'
import { UserService } from './../../providers/user';

@IonicPage()
@Component({
  selector: 'page-subscription-prompt',
  templateUrl: 'subscription-prompt.html',
})
export class SubscriptionPromptPage {
  public loader: Loading;
  public weeklyProduct: IAPProduct
  public unlimitedProduct: IAPProduct
  public wrongsubscription: IAPProduct
  public testsubscription: IAPProduct
  public nosubscription: IAPProduct
  public weeklysub: string
  public unlimitedsub: string
  public registrationCalls = new Subject<string>()
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private userService: UserService,
    private config: Config,
    private store: InAppPurchase2,
    private storage: Storage,
    public plt: Platform
  ) {
    console.log('SubscriptionPromptPage::construct');
    // should clear user on storage so we can force user to relogin after here
    if (this.plt.is('ios')) {
      // This will only print when on iOS
      console.log('I am an iOS device!');
    };
    if (this.plt.is('android')) {
      // This will only print when on iOS
      // this.store.sandbox = true;
      console.log('SubscriptionPromptPage::I am an android device!');
    };
    this.store.verbosity = this.store.ERROR;
    this.storage.remove('user');
    this.loader = this.loadingCtrl.create({ dismissOnPageChange: true });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad::SubscriptionPromptPage');
    // debouncing the queue to limit extra calls
    this.registrationCalls
      .debounceTime(1000)
      .subscribe(type => {
        console.log('SubscriptionPromptPage::registrationCalls::subscribe::type[' + type + ']');
        let subscription = { weekly: false, unlimited: false }

        this.storage.get('subscription').then(osubscription => {
          console.log('SubscriptionPromptPage::registrationCalls::Current Sub in storage=', JSON.stringify(osubscription));
          subscription = osubscription;

          // if (this.plt.is('android')) {
          //   if (type === 'weekly_subscription') {
          //     console.log('SubscriptionPromptPage::registrationCalls::subscribe::weekly is true');
          //     subscription.weekly = true;
          //   }
          //   if (type === 'unlimited_subscription') {
          //     console.log('SubscriptionPromptPage::registrationCalls::subscribe::unlimited is true');
          //     subscription.unlimited = true;
          //   }
          //   if (type === 'weekly_subscription_off') {
          //     console.log('SubscriptionPromptPage::registrationCalls::subscribe::weekly is false');
          //     subscription.weekly = false;
          //   }
          //   if (type === 'unlimited_subscription_off') {
          //     console.log('SubscriptionPromptPage::registrationCalls::subscribe::unlimited is false');
          //     subscription.unlimited = false;
          //   }
          // }
          // else {
          if (type === 'weekly') {
            console.log('SubscriptionPromptPage::registrationCalls::subscribe::weekly is true');
            subscription.weekly = true;
          }
          if (type === 'unlimited') {
            console.log('SubscriptionPromptPage::registrationCalls::subscribe::unlimited is true');
            subscription.unlimited = true;
          }
          if (type === 'weekly_off') {
            console.log('SubscriptionPromptPage::registrationCalls::subscribe::weekly is false');
            subscription.weekly = false;
          }
          if (type === 'unlimited_off') {
            console.log('SubscriptionPromptPage::registrationCalls::subscribe::unlimited is false');
            subscription.unlimited = false;
          }
          // }

          this.storage.set('subscription', subscription)
            .then(() => {
              if (subscription.unlimited == true || subscription.weekly == true) {
                console.log('************  SET STORAGE FOR SUBSCRIPTION ************')
                this.userService.registerNewProfile()
                  .subscribe(() => {
                    console.log('SubscriptionPromptPage::registrationCalls::subscribe:: GOTO Home page ie valid subscription found');
                    this.userService.resetNavigationTo('home')
                  }, (err) => console.error(err))
              }
              else {
                console.log('SubscriptionPromptPage::registrationCalls::subscribe:: SUBSCRIPTION NOT FOUND, HENCE STAY HERE');
              }
            })

        })

      })



    try {
      if (this.plt.is('android')) {
        console.log('ionViewDidLoad::SubscriptionPromptPage::android...CHECK IF ANY VALID SUBSCRPTION');
        this.weeklysub = "weekly_subscription";
        this.unlimitedsub = "unlimited_subscription";

        console.log('Store is Ready:this.store.ready() ' + JSON.stringify(this.store.ready()));
        console.log('$$$$$$$$$$$$$$Store Products: ' + JSON.stringify(this.store.products));
        this.weeklyProduct = this.store.get(this.weeklysub);
        console.warn('SubscriptionPromptPage[android]::registrationCalls::get weeklyProduct..', JSON.stringify(this.weeklyProduct));
        this.unlimitedProduct = this.store.get(this.unlimitedsub);
        console.warn('SubscriptionPromptPage[android]::registrationCalls::get unlimitedProduct', JSON.stringify(this.unlimitedProduct));
        this.store.validator = this.config.API_URL + '/itunes/validate-purchase';
        // this.store.ready().then((status) => {
        //   console.log('store_ready PROMISE');
        // }).catch(function onCatch(response) {
        //   console.error('SubscriptionPromptPage[android]::registrationCalls::android::response', JSON.stringify(response));
        // });
      }
      else {
        this.weeklysub = 'com.jaypiggin.iamelite.weekly3';
        this.unlimitedsub = 'com.jaypiggin.iamelite.unlimited1';
        // this.store.ready().then((status) => {
        //   console.log('store_ready', {});
        // console.log(JSON.stringify(this.store.get(this.weeklysub)));
        //  console.log(JSON.stringify(this.store.get(this.unlimitedsub)));
        console.log('ios::Store is Ready: ' + JSON.stringify(status));
        console.log('$$$$$$$$$$$$$$$$$$$$$Products:ios ' + JSON.stringify(this.store.products));
        this.weeklyProduct = this.store.get(this.weeklysub);
        console.log('222SubscriptionPromptPage[android]::registrationCalls::get weekly..', JSON.stringify(this.weeklyProduct));
        this.unlimitedProduct = this.store.get(this.unlimitedsub);
        console.log('333SubscriptionPromptPage[android]::registrationCalls::get unlimited', JSON.stringify(this.unlimitedProduct));
        this.store.validator = this.config.API_URL + '/itunes/validate-purchase';
        // }).catch(function onCatch(response) {
        //   console.error('SubscriptionPromptPage[ios]::registrationCalls::ios::response', JSON.stringify(response));
        // });
      }

      this.store.once(this.unlimitedsub).approved((product: IAPProduct) => {
        console.log('@@@@@@@@@@@@@SubscriptionPromptPage :: approved', JSON.stringify(product))
        if (product.owned == true || product.state == "approved") {
          console.log('SubscriptionPromptPage :: unlimitedProduct approved or OWNED', JSON.stringify(product))
          //this.setsubscription(this.unlimitedsub);
          product.finish();
          this.registrationCalls.next("unlimited"); // queue it
          console.log('SubscriptionPromptPage :: unlimitedProduct approved or OWNED..GOING HOME')
          this.loader.dismiss();
          this.userService.resetNavigationTo('home');
        }
        else {
          this.registrationCalls.next("unlimited_off");
          console.log('SubscriptionPromptPage :: unlimited not approved or owned hence clear IT');
        }
      })
      this.store.once(this.weeklysub).approved((product: IAPProduct) => {
        console.log('@@@@@@@@@@@@@SubscriptionPromptPage :: weeklysub approved', JSON.stringify(product))
        if (product.owned == true || product.state == "approved") {
          console.log('SubscriptionPromptPage ::weeklysub approved or OWNED', JSON.stringify(product))
          //this.setsubscription(this.weeklysub);
          product.finish();
          this.registrationCalls.next("weekly"); // queue it
          console.log('SubscriptionPromptPage :: weeklysub approved or OWNED..GOING HOME')
          this.loader.dismiss();
          this.userService.resetNavigationTo('home');
        }
        else {
          this.registrationCalls.next("weekly_off");
          console.log('SubscriptionPromptPage :: Weekly not approved or owned hence clear IT');
        }
        //   product.verify();
        // })
        // this.store.once('weekly').verified((product: IAPProduct) => {
        //   console.log('SubscriptionPromptPage :: verified', JSON.stringify(product))
      })

      this.store.when(this.weeklysub).updated((product: IAPProduct) => {
        console.log('++++++++++++++weeklyProduct:updated STATE[' + JSON.stringify(product.state) + "]");
      });
      this.store.when(this.unlimitedsub).updated((product: IAPProduct) => {
        console.log('++++++++++++++unlimitedProduct::updated STATE[' + JSON.stringify(product.state) + "]");
      });

      this.store.when(this.unlimitedsub).registered((product: IAPProduct) => {
        console.log('^^^^^^^^^^^^unlimitedProduct::registered: ' + JSON.stringify(product));
      });
      this.store.when(this.weeklysub).registered((product: IAPProduct) => {
        console.log('^^^^^^^^^^^^Registered:weeklyProduct ' + JSON.stringify(product));
      });

      this.store.when(this.weeklysub).cancelled((product: IAPProduct) => {
        console.log('------------SubscriptionPromptPage ::weeklyProduct cancelled', JSON.stringify(product))
        this.loader.dismiss();
      })
      this.store.when(this.unlimitedsub).cancelled((product: IAPProduct) => {
        console.log('------------SubscriptionPromptPage ::unlimitedsub cancelled', JSON.stringify(product))
        this.loader.dismiss();
      })

      this.store.error(err => {
        console.error('SubscriptionPromptPage :: Store error: ' + JSON.stringify(err))
        this.loader.dismiss();
        alert('SubscriptionPromptPage :: Store error: ' + JSON.stringify(err));
        this.userService.resetNavigationTo('front');
      })

    } catch (err) {
      console.error('Store approved listener error: ' + JSON.stringify(err))
      this.loader.dismiss();
      this.userService.resetNavigationTo('front');
      //alert('Store approved listener error: ' + JSON.stringify(err));
    }
    console.log('SubscriptionPromptPage ::refresh Store..')
    this.store.refresh();
  }

  checkit(): void {
    console.log('checkit..refresh store');
    this.store.refresh();
  }


  // setsubscription(type: string): void {
  //   let subscription = { weekly: false, unlimited: false }

  //   if (type === this.weeklysub) {
  //     console.log('SubscriptionPromptPage::registrationCalls::setsubscription::weekly is true');
  //     subscription.weekly = true;
  //   }
  //   if (type === this.unlimitedsub) {
  //     console.log('SubscriptionPromptPage::registrationCalls::setsubscription::unlimited is true');
  //     subscription.unlimited = true;
  //   }

  //   this.storage.set('subscription', subscription)
  //     .then(() => {
  //       console.log('************  SET STORAGE FOR SUBSCRIPTION ************')
  //       this.userService.registerNewProfile()
  //         .subscribe(() => {
  //           console.log('SubscriptionPromptPage::registrationCalls::subscribe::registerNewProfile successfull');
  //         }, (err) => console.error(err))
  //     })

  // }







  subscribe(type: string): void {
    console.log('SubscriptionPromptPage::subscribe type[' + type + "]");
    this.loader = this.loadingCtrl.create({ dismissOnPageChange: true });
    this.loader.present();
    try {
      if (type === 'unlimited') {
        this.store.order(this.unlimitedsub).then(() => {
          console.log('SubscriptionPromptPage::subscribe::Unlimited Purchase successful!')
        })
      } else {
        this.store.order(this.weeklysub).then(() => {
          console.log('SubscriptionPromptPage::subscribe::Weekly Purchase successful!')
        })
      }
    } catch (err) {
      console.error('Store order error:[GOTO FRONT PAGE] ' + JSON.stringify(err))
      this.loader.dismiss();
      this.userService.resetNavigationTo('front');
    }
  }

  close(): void {
    console.log('close');
    this.userService.resetNavigationTo('login');
  }

  restorePurchase(): void {
    console.log('SubscriptionPromptPage::restorePurchase');
    this.loader = this.loadingCtrl.create({ dismissOnPageChange: true });
    this.loader.present();
    try {
      this.store.refresh();
      this.loader.dismiss();
    } catch (err) {
      console.log('Store refresh/restore error[GOTO TO FRONT PAGE]: ' + JSON.stringify(err))
      this.loader.dismiss();
      this.userService.resetNavigationTo('front');
    }
  }

  gotoTermsPage(): void {
    this.navCtrl.push('FrontTermsPage');
  }

  gotoPrivacyPage(): void {
    this.navCtrl.push('FrontPrivacyPage');
  }

  gotoAboutSubscriptionPage(): void {
    this.navCtrl.push('FrontAboutSubscriptionPage');
  }

  gotoFrontPage(): void {
    this.navCtrl.push('FrontPage');
  }
}
