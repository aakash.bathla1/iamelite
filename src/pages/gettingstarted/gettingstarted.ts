import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, LoadingController, Loading, NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { UserService } from '../../providers/user';
import { User } from './../../shared/user';

@IonicPage()
@Component({
  selector: 'page-gettingstarted',
  templateUrl: 'gettingstarted.html',
})
export class GettingStartedPage {
  public weeks: any[];
  public currentWeek: number;
  public profileForm: FormGroup;
  public loader: Loading;
  @ViewChild('file') fileInput: ElementRef;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public userService: UserService,
    private fb: FormBuilder,
    private loadingCtrl: LoadingController
  ) {
    this.profileForm = this.fb.group({
      email: ['', Validators.required],
      name: ['', Validators.required],
      height: [''],
      startingWeight: [''],
      currentWeight: [''],
      goalWeight: [''],
      age: [''],
      timezone: [''],
      showGettingStarted: [false]
    });
  }

  ionViewWillEnter() {
    this.userService.getSelf()
    .subscribe((user: User) => {
      this.weeks = user.weeks;
      this.profileForm.setValue({
        email: user.email,
        name: user.name,
        height: user.height,
        age: user.age,
        startingWeight: user.startingWeight,
        currentWeight: user.currentWeight,
        goalWeight: user.goalWeight,
        timezone: user.timezone,
        showGettingStarted: false
      });
    });
  }

  showFileBrowser(): void {
    this.fileInput.nativeElement.click();
  }

  onFileChange(files:FileList): void {
    console.log(files);
    this.loader = this.loadingCtrl.create();
    this.loader.present();
    this.userService.uploadPhoto(files[0])
      .subscribe((user: User) => {
        this.loader.dismiss();
      }, error => this.loader.dismiss())
  }

  onSubmit() {
    this.userService.updateProfile(this.profileForm.value)
      .subscribe(user => {
        this.loader.dismiss();
        this.userService.resetNavigationTo('home');
      })
      ;

    this.loader = this.loadingCtrl.create();
    this.loader.present();
  }

}
