import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';

import { UserService } from '../../providers/user';

@IonicPage()
@Component({
  selector: 'page-front',
  templateUrl: 'front.html',
})
export class FrontPage {
  public profile: FormGroup;
  public loader: Loading;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private fb: FormBuilder,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private userService: UserService
  ) {
    this.profile = this.fb.group({
      email: ['', Validators.email],
      name: ['', Validators.required]
    });
  }

  onSubmit() {
    if (this.profile.invalid) {
      console.log('FrontPage::onSubmit::invalid profile');
      let error = 'Unable to proceed. All fields are required.';
      if (this.profile.get('name').valid && this.profile.get('email').invalid) error = 'Unable to proceed. Email is not valid.'

      let info = this.toastCtrl.create({
        message: error,
        position: 'top',
        duration: 3000
      });
      info.present();

      return;
    }
    this.userService.preRegister(this.profile.value)
      .subscribe((user) => {
        this.loader.dismiss();
        this.userService.resetNavigationTo('subscription');
      }, error => {
        this.loader.dismiss();
        let info = this.toastCtrl.create({
          message: error.message,
          position: 'top',
          duration: 3000
        });
        info.present();
      });
    
    this.loader = this.loadingCtrl.create({ dismissOnPageChange: true});
    this.loader.present();
  }

  gotoLoginPage(e) {
    console.log('FrontPage::gotoLoginPage');
    e.preventDefault();
    this.userService.resetNavigationTo('login')
  }

  gotoTermsPage(): void {
    console.log('FrontPage::gotoTermsPage');    
    this.navCtrl.push('FrontTermsPage');
  }

  gotoPrivacyPage(): void {
    console.log('FrontPage::gotoPrivacyPage');    
    this.navCtrl.push('FrontPrivacyPage');
  }

  gotoAboutSubscriptionPage(): void {
    console.log('FrontPage::gotoAboutSubscriptionPage');
    this.navCtrl.push('FrontAboutSubscriptionPage');
  }

}
