import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-education',
  templateUrl: 'education.html',
})
export class EducationPage {
  public nutritionRoot:any = 'EducationNutritionPage';
  public trainingRoot:any = 'EducationTrainingPage';
  public workoutRoot:any = 'EducationWorkoutPage';
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}
