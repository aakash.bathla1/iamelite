import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { UserService } from './../../providers/user';

@IonicPage()
@Component({
  selector: 'page-vitamins',
  templateUrl: 'vitamins.html',
})
export class VitaminsPage {
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private userService: UserService,
  ) {
  }

  gotoHomePage(): void {
    this.userService.resetNavigationTo('home');
  }

}
