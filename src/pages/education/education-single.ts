import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer} from '@angular/platform-browser';

@IonicPage()
@Component({
  selector: 'page-education-single',
  templateUrl: 'education-single.html',
})
export class EducationSinglePage {
  public type: string;
  public content: any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private domSanitizer: DomSanitizer,
  ) {
    this.type = navParams.get('type');
    this.content = navParams.get('content');
  }

  ionViewDidLoad() {
    this.content.imgSource = this.domSanitizer.bypassSecurityTrustResourceUrl(this.content.source);
    this.content.videoSource = this.domSanitizer.bypassSecurityTrustResourceUrl(this.content.source);
    this.content.preview = this.domSanitizer.bypassSecurityTrustResourceUrl(this.content.preview);
  }
}
