import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SupplementsPage } from './supplements';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    SupplementsPage,
  ],
  imports: [
    IonicPageModule.forChild(SupplementsPage),
    ComponentsModule
  ],
})
export class SupplementsPageModule {}
