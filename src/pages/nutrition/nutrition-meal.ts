import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer} from '@angular/platform-browser'

import { ContentService } from '../../providers/content';
import { UserService } from '../../providers/user';
import { Content } from '../../shared/content';

@IonicPage()
@Component({
  selector: 'page-nutrition-meal',
  templateUrl: 'nutrition-meal.html',
})
export class NutritionMealPage {
  public contents: Content[];
  public currentWeek: number;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public domSanitizer: DomSanitizer,
    private userService: UserService,
    private contentService: ContentService,
  ) {
    this.currentWeek = this.userService.currentWeek;
    this.contents = [];
  }

  ionViewWillEnter() {
    
    this.updateContent();

    this.userService.currentWeek$
      .subscribe(week => this.updateContent())
  }

  updateContent(): void {
    this.contentService
      .getWeeklyMealPlans(this.userService.currentWeek)
      .subscribe(data => {
        this.contents = data.map(content => {
          content.styleUrl = this.domSanitizer.bypassSecurityTrustStyle('url(' + content.preview + ')');
          return content;
        })
      })
      ;
  }

  gotoHomePage(): void {
    this.userService.resetNavigationTo('home');
  }

  viewPage(content): void {
    this.navCtrl.push('NutritionMealDetailPage', {content: content});
  }

}
