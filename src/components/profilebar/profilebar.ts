import { Component, Output, EventEmitter, Input } from '@angular/core';
import { NavController, Loading, LoadingController } from 'ionic-angular';

import { UserService } from '../../providers/user';
import { User } from '../../shared/user';

@Component({
  selector: 'profilebar',
  templateUrl: 'profilebar.html'
})
export class ProfilebarComponent {
  @Output() onCurrentWeekChanged = new EventEmitter<number>();
  @Input() showWeeks: boolean;
  public loader: Loading;
  public weeks: any[];
  public currentWeek: number;
  public user: User;

  constructor(
    private userService: UserService,
    private navCtrl: NavController,
    public loadingCtrl: LoadingController
  ) {
    console.log('ProfilebarComponent');
    this.showWeeks = true;
    this.user = new User();
    this.weeks = [];
    this.userService.getSelf()
      .subscribe((user: User) => {
        this.weeks = user.weeks;
        this.user = user;
      });
    this.currentWeek = this.userService.currentWeek;
    this.userService.currentWeek$.subscribe(week => this.currentWeek = week);
  }

  ionViewDidEnter() {
    this.currentWeek = this.userService.currentWeek;
  }

  updateCurrentWeek(week: number) {
    this.onCurrentWeekChanged.emit(week);
    this.userService.updateCurrentWeek(week);
  }

  gotoProfilePage(): void {
    this.navCtrl.push('AccountPage', {user: this.user}).then(() => this.loader.dismiss());

    this.loader = this.loadingCtrl.create({ dismissOnPageChange: false});
    this.loader.present();
  }

  gotoHomePage(): void {
    this.userService.resetNavigationTo('home');
  }
}
