import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EducationTrainingPage } from './education-training';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    EducationTrainingPage,
  ],
  imports: [
    IonicPageModule.forChild(EducationTrainingPage),
    ComponentsModule,
  ],
})
export class EducationTrainingPageModule {}
