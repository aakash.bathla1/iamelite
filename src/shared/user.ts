export class User {
  public id: string
  public name: string
  public email: string
  public photo: string
  public age: string
  public timezone: string
  public height: string
  public startingWeight: string
  public goalWeight: string
  public currentWeight: string
  public subscription: any
  public showGettingStarted: boolean
  public accessToken: string
  public weeks: number[]
  
  constructor(obj?: any) {
    this.id = obj && obj.id || null;
    this.name = obj && obj.name || null;
    this.email = obj && obj.email || null;
    this.photo = obj && obj.profile_photo || null;
    this.age = obj && obj.age || null;
    this.timezone = obj && obj.timezone || null;
    this.height = obj && obj.height || null;
    this.startingWeight = obj && obj.starting_weight || null;
    this.goalWeight = obj && obj.goal_weight || null;
    this.currentWeight = obj && obj.current_weight || null;
    this.subscription = obj && obj.subscription || null;
    this.showGettingStarted = (obj && obj.getting_started_done !== false) ? false : true;
    this.accessToken = obj && obj.access_token || null;
    this.weeks = obj && obj.weeks || [];
  }
}